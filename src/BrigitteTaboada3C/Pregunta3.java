/*3.Crear un programa que lea cantidades y precios y al final indique el total de la factura. 
Primero se pregunta: 
Introduzca la cantidad vendida tras lo cual el usuario introducirá un número entero positivo.
Después se pregunta:
Introduzca el precio que será un número decimal positivo.
La lectura termina cuando en la cantidad se introduzca un cero. Si es así se escribirá el total.*/

package BrigitteTaboada3C;
import java.util.Scanner;

public class Pregunta3 {
    public static void main(String[] args){
        double valor, resultado=0;
        Scanner teclado=new Scanner(System.in);
        int N = 0;
        do{
            do{
                System.out.print("Introduzca la cantidad vendida: ");
                N=teclado.nextInt();
                if(N<0)
                    System.out.println("Cantidad no valida");
            }while(N<0);
            if (N>0){
                System.out.print("Introduzca el valor : ");
                do{
                    valor=teclado.nextInt();
                    if(valor<0) 
                        System.out.println("El valor no es valido"); 
                    else resultado=N*valor;
                }while(valor<0);
            }
        }while(N!=0); 
        System.out.print("Total vendido= "+resultado);
    }
}
    
