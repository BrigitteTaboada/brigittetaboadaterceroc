/*2.Crear un programa que lea un número entero y a partir de él cree un cuadrado de 
asteriscos con ese tamaño. Los asteriscos sólo se verán en el borde del cuadrado, no en el interior.*/

package BrigitteTaboada3C;
import java.util.Scanner;

public class Pregunta2 {
     public static void main(String[]args){
        Scanner teclado=new Scanner(System.in);
        System.out.println("Ingresar un numero:");
        int c=teclado.nextInt();
        for (int j = 0; j < c; j++) {
            System.out.print(" *");
        }
        System.out.println("");
        
        for (int i = 0; i < c-2; i++) {
          System.out.print(" *");

        for (int j = 0; j < c-2; j++) {
            System.out.print("  ");
        }
        System.out.println(" *");
        }
        
        for (int j = 0; j < c; j++) {
            System.out.print(" *");
        }
        System.out.println("");
    }
    
}



