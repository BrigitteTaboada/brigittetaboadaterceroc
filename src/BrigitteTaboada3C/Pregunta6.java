//6.Modificar el ejercicio anterior para que aparezca un rombo. 

package BrigitteTaboada3C;
import java.util.Scanner;

public class Pregunta6 {
    public static void main(String[] args) {
        Scanner teclado=new Scanner(System.in);
        System.out.println("Ingresar un numero:");
        int r=teclado.nextInt();
        for (int i = 0; i <= r; i++) {
            for (int j = r-i; j > 0;j--) {
                System.out.print(" ");
            }
            for (int j = 0; j < i; j++) {
                System.out.print(" *");
            }
            System.out.println("");
        }
        for (int i = 0; i <= r; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(" ");
            }
            for (int j = r-i-1; j > 0; j--) {
                System.out.print(" *");
            }
            System.out.println("");
         
        }
          
    }
    
}
